#!/bin/bash

TEST= 'whoami'  #test si on est en root
if [ "$TEST" != "root" ]; 

then

	echo "[...] update database [...]"
	apt-get update             #On est en root donc, On fait les mise à jour

	echo "[...] upgrade system [...]"
	apt-get upgrade		   #On est en root donc, On fait les mise à jour	
else

	echo " [/!\] Vous devez être super-utilisateur [/!\] " #L'utilisateur doit paasser en super-utilisateur


fi
